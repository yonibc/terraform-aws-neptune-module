data "aws_caller_identity" "current" {}

locals {
  name = replace(var.name, "_", "-")
}

resource "aws_neptune_cluster" "default" {
  cluster_identifier                  = "${local.name}-neptune-cluster"
  engine                              = "neptune"
  backup_retention_period             = 5
  preferred_backup_window             = "07:00-09:00"
  skip_final_snapshot                 = true
  iam_database_authentication_enabled = false
  apply_immediately                   = true
  neptune_subnet_group_name           = aws_neptune_subnet_group.default.name
  vpc_security_group_ids              = concat([aws_security_group.neptune_security_group.id], var.vpc_security_group_ids)
  iam_roles                           = [aws_iam_role.neptune_load_role.arn]
}

resource "aws_neptune_cluster_instance" "default" {
  count              = var.cluster_count
  cluster_identifier = aws_neptune_cluster.default.id
  engine             = "neptune"
  instance_class     = var.cluster_instance_class
  apply_immediately  = true
  neptune_parameter_group_name = aws_neptune_parameter_group.covid.name
}

resource "aws_neptune_subnet_group" "default" {
  name       = "${local.name}-neptune-main"
  subnet_ids = var.neptune_subnets
}

resource "aws_neptune_parameter_group" "covid" {
  family = "neptune1"
  name   = "${local.name}-neptune-pg"

  parameter {
    name  = "neptune_query_timeout"
    value = "180000"
  }
}

resource "aws_security_group" "neptune_security_group" {
  name        = "${local.name}-neptune-vpc-access"
  description = "Allow Neptune inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "Neptune from VPC ipv4"
    from_port   = 8182
    to_port     = 8182
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Neptune from VPC ipv6"
    from_port   = 8182
    to_port     = 8182
    protocol    = "tcp"
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_iam_role" "neptune_load_role" {
  name = "${local.name}-neptune-load-role"
  assume_role_policy = data.aws_iam_policy_document.neptune_assume_access.json
}

resource "aws_iam_role_policy_attachment" "neptune_load_role_attachement" {
  role       = aws_iam_role.neptune_load_role.name
  policy_arn = aws_iam_policy.neptune_load.arn
}

data "aws_iam_policy_document" "neptune_assume_access" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "neptune_load" {
  name        = "${local.name}-neptune-load"
  description = "Neptune access to S3 for loading data"
  policy = data.aws_iam_policy_document.neptune_load.json
}

data "aws_iam_policy_document" "neptune_load" {
  statement {
    actions = [
      "s3:ListAllMyBuckets",
      "s3:HeadBucket",
    ]

    resources = [
      "*",
    ]
  }
  statement {
    actions = [
      "s3:List*",
      "s3:Get*",
    ]

    resources = [
      "*",
    ]
  }
}
