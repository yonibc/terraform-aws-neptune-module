output "neptune_cluster_id" {
  value = aws_neptune_cluster.default.id
}

output "neptune_arn" {
  value = aws_neptune_cluster.default.arn
}

output "neptune_endpoint" {
  value = aws_neptune_cluster.default.endpoint
}

output "neptune_reader_endpoint" {
  value = aws_neptune_cluster.default.reader_endpoint
}

output "neptune_notebook_cluster_id" {
  value = aws_sagemaker_notebook_instance.neptune_notebook_instance.id
}
