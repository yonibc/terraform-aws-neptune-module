## terraform-aws-module
This module stands up a base Neptune cluster.

It requires an existing VPC to launch the cluster in.

It creates the same security groups and default parameter groups as created by the AWS console.

It provides variables to set instance types for Cluster instances.
It also allows the setting of number of instances in the Cluster.