variable "name" {
  type        = string
  description = "Name to prefix module resources with"
}

variable "aws_region" {
  type        = string
  description = "Region to place instance"
  default = "us-east-1"
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC in which to provision the AWS resources"
}

variable "vpc_security_group_ids" {
  type        = list(string)
  description = "List of security groups for Neptune cluster"
  default = []
}

variable "neptune_subnets" {
  type        = list(string)
  description = "List of subnets to place EC2 instances"
}

variable "cluster_count" {
  type        = number
  description = "Number of instances in the Neptune Cluster"
  default     = 1
}

variable "cluster_instance_class" {
  type        = string
  description = "Instance type of Neptune cluster machines eg. db.t3.medium"
  default     = "db.t3.medium"
}
